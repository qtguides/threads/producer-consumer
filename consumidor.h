#ifndef CONSUMIDOR_H
#define CONSUMIDOR_H

#include <QThread>
#include "buffer.h"

class Consumidor : public QThread
{
    Q_OBJECT

public:
    Consumidor(Buffer * buffer, QString nombre, int cantidad, QObject * parent = nullptr);
    void run() override;

private:
    int cantidad;
    QString nombre;
    Buffer *buffer;
};

#endif // CONSUMIDOR_H
