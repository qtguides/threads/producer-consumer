#include "mainwindow.h"
#include <QApplication>
#include "productor.h"
#include "consumidor.h"
#include "buffer.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    Buffer buffer(&w);
    Productor  p1(&buffer, "P1", 400);
    Productor  p2(&buffer, "P2", 400);
    Productor  p3(&buffer, "P3", 200);
    Consumidor c1(&buffer, "C1", 500);
    Consumidor c2(&buffer, "C2", 500);

    c1.start();
    p1.start();
    c2.start();
    p2.start();
    p3.start();

    p1.wait();
    p2.wait();
    p3.wait();
    c1.wait();
    c2.wait();

    return a.exec();
}
