#include <QDebug>
#include "productor.h"
#include "item.h"

Productor::Productor(Buffer *buffer, QString nombre, int cantidad, QObject *parent)
    : QThread(parent),
      cantidad(cantidad),
      nombre(nombre),
      buffer(buffer)
{

}

void Productor::run()
{
    int i;

    for(i = 0; i < cantidad; i++)
    {
        Item * item = new Item(nombre, i);
        buffer->pushItem(item, nombre);
    }
}
