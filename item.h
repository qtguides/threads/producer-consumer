#ifndef ITEM_H
#define ITEM_H

#include <QObject>
#include <QDebug>

class Item  : public QObject
{
    Q_OBJECT

public:
    Item(QString &nom, int num, QObject *parent = nullptr);
    friend QDebug& operator<<( QDebug& out, Item const &i);

private:
    QString nombre;
    int numero;
};
#endif // ITEM_H
