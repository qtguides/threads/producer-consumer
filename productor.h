#ifndef PRODUCTOR_H
#define PRODUCTOR_H

#include <QThread>
#include "buffer.h"

class Productor : public QThread
{
    Q_OBJECT

public:
    Productor(Buffer * buffer, QString nombre, int cantidad, QObject * parent = nullptr);
    void run() override;

private:
    int cantidad;
    QString nombre;
    Buffer *buffer;
};

#endif // PRODUCTOR_H
