#include <QDebug>
#include "consumidor.h"
#include "item.h"

Consumidor::Consumidor(Buffer *buffer, QString nombre, int cantidad, QObject *parent)
    : QThread(parent),
      cantidad(cantidad),
      nombre(nombre),
      buffer(buffer)
{

}

void Consumidor::run()
{
    int i;

    for(i = 0; i < cantidad; i++)
    {
        Item * item = buffer->popItem(nombre);
        // usar el elemento para algo
        delete item;
    }
}
