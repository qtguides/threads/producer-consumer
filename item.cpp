#include "item.h"

Item::Item(QString &nom, int num, QObject *parent)
    : QObject (parent),
      nombre(nom),
      numero(num)
{

}

QDebug& operator<<( QDebug& out, Item const &i)
{
    return out << QString("[") + i.nombre + QString(" ") +QString::number(i.numero)+"]";
}
