#include "buffer.h"

Buffer::Buffer(QObject *parent)
    : QObject(parent),
      mutex(QMutex::NonRecursive),
      pushSem(BUFFER_ELEMENTS),
      popSem(0)
{
    pushIndex = 0;
    popIndex = 0;
    memset(buffer, 0, BUFFER_ELEMENTS * sizeof(Item*));
}

void Buffer::pushItem(Item * item, QString nombre)
{
    pushSem.acquire();

    mutex.lock();
    buffer[pushIndex] = item;
    pushIndex = (pushIndex + 1) % BUFFER_ELEMENTS;
    qDebug() << (nombre+":") << *item << "count:" << popSem.available();
    mutex.unlock();

    popSem.release();
}

Item * Buffer::popItem(QString nombre)
{
    Item * ret;

    popSem.acquire();

    mutex.lock();
    ret = buffer[popIndex];
    popIndex = (popIndex + 1) % BUFFER_ELEMENTS;
    qDebug() << (nombre+":") << *ret << ("count:" + QString::number(popSem.available()));
    mutex.unlock();

    pushSem.release();

    return ret;
}
