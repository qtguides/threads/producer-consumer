#ifndef BUFFER_H
#define BUFFER_H

#include <QObject>
#include <QMutex>
#include <QSemaphore>
#include "item.h"

#define BUFFER_ELEMENTS 10

class Buffer : public QObject
{
    Q_OBJECT

public:
    explicit Buffer(QObject *parent = nullptr);
    void pushItem(Item * item, QString nombre);
    Item * popItem(QString nombre);

signals:

public slots:

private:
    int pushIndex;
    int popIndex;
    Item * buffer[BUFFER_ELEMENTS];

    QMutex mutex;
    QSemaphore pushSem;
    QSemaphore popSem;
};

#endif // BUFFER_H
